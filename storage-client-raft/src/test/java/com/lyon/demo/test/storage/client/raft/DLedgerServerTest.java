package com.lyon.demo.test.storage.client.raft;

import cn.hutool.core.thread.ThreadUtil;
import com.lyon.dmeo.storage.client.raft.DLedgerServer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author LeeYan9
 * @since 2022-05-31
 */
@Slf4j
public class DLedgerServerTest extends DLedgerBaseUnitTest {

    @SneakyThrows
    @Test
    public void test_three_server_start() {
        AtomicInteger leaderNum = new AtomicInteger(0);
        AtomicInteger followerNum = new AtomicInteger(0);
        ArrayList<DLedgerServer> servers = Lists.newArrayList();
        servers.add(launchServer(group, peers, "n0"));
        servers.add(launchServer(group, peers, "n1"));
        servers.add(launchServer(group, peers, "n2"));
        ThreadUtil.sleep(3000);
        DLedgerServer leaderServer = parseServers(servers, leaderNum, followerNum);
        Assert.assertEquals(1, leaderNum.get());
        Assert.assertEquals(2, followerNum.get());
        Assert.assertNotNull(leaderServer);

        // 初始化数据比较 COMPARE => TRUNCATE
        commit(leaderServer, "13213");

        // APPEND
        commit(leaderServer, "山大倒萨倒萨阿斯顿撒");

        commitEntry(leaderServer, null, "123", "123");
        commitEntry(leaderServer, null, "123", "456");

    }


}
