package com.lyon.dmeo.storage.client.raft.core.proccessor;

import com.lyon.demo.storage.client.api.core.command.request.HeartbeatRequest;
import com.lyon.demo.storage.client.api.core.command.request.PushEntryRequest;
import com.lyon.demo.storage.client.api.core.command.request.VoteRequest;
import com.lyon.demo.storage.client.api.core.command.response.HeartBeatResponse;
import com.lyon.demo.storage.client.api.core.command.response.PushEntryResponse;
import com.lyon.demo.storage.client.api.core.command.response.VoteResponse;

import java.util.concurrent.CompletableFuture;

/**
 * @author LeeYan9
 * @since 2022-05-30
 */
@SuppressWarnings({"rawtypes", "AlibabaClassNamingShouldBeCamel"})
public interface DLedgerRaftClientProtocol {

    /**
     * 发送心跳处理
     *
     * @param heartBeatRequest 请求
     * @return 返回
     */
    CompletableFuture<HeartBeatResponse> heartBeat(HeartbeatRequest heartBeatRequest);

    /**
     * 发送日志请求
     *
     * @param pushEntryRequest 请求
     * @return 返回
     */
    CompletableFuture<PushEntryResponse> push(PushEntryRequest pushEntryRequest);

    /**
     * 发送投票请求
     *
     * @param voteRequest 请求
     * @return 返回
     */
    CompletableFuture<VoteResponse> vote(VoteRequest voteRequest);

//    CompletableFuture<PullEntriesResponse> pull(PullEntriesRequest request) throws Exception;

}
