package com.lyon.dmeo.storage.client.raft.core.proccessor;

import cn.hutool.json.JSONUtil;
import com.lyon.demo.protocol.netty.command.RemoteCommand;
import com.lyon.dmeo.storage.client.raft.core.proccessor.core.AbstractRequestProcessor;
import lombok.SneakyThrows;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * example http connection
 *
 * @author LeeYan9
 * @since 2022-05-27
 */
@SuppressWarnings({"rawtypes", "AlibabaClassNamingShouldBeCamel"})
public abstract class AbstractHttpRequestProcessor extends AbstractRequestProcessor<OutputStream, RemoteCommand> {

    @SneakyThrows
    @Override
    public RemoteCommand processRequest(OutputStream ctx, RemoteCommand remoteCommand) {

        RemoteCommand response = processRequest(remoteCommand);
        byte[] bytes = JSONUtil.toJsonStr(response).getBytes(StandardCharsets.UTF_8);
        ctx.write(bytes);
        return response;
    }
}
