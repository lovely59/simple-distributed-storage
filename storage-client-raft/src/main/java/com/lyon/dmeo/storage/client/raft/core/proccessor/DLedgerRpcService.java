package com.lyon.dmeo.storage.client.raft.core.proccessor;

/**
 * @author LeeYan9
 * @since 2022-05-30
 */
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public interface DLedgerRpcService extends DLedgerRaftProtocolHandler,DLedgerRaftClientProtocol{
}
