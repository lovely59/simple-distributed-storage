package com.lyon.dmeo.storage.client.raft.core.proccessor;

import com.lyon.demo.storage.client.api.core.command.request.HeartbeatRequest;
import com.lyon.demo.storage.client.api.core.command.request.PushEntryRequest;
import com.lyon.demo.storage.client.api.core.command.request.VoteRequest;
import com.lyon.demo.storage.client.api.core.command.response.HeartBeatResponse;
import com.lyon.demo.storage.client.api.core.command.response.PushEntryResponse;
import com.lyon.demo.storage.client.api.core.command.response.VoteResponse;

import java.util.concurrent.CompletableFuture;


/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@SuppressWarnings({"AlibabaClassNamingShouldBeCamel"})
public interface DLedgerRaftProtocolHandler {

    /**
     * 心跳处理
     *
     * @param heartbeatRequest 请求
     * @return 返回
     */
    CompletableFuture<HeartBeatResponse> handleHeartBeat(HeartbeatRequest heartbeatRequest);

    /**
     * 日志请求处理
     *
     * @param entryRequest 请求
     * @return 返回
     */
    CompletableFuture<PushEntryResponse> handlePush(PushEntryRequest entryRequest);

    /**
     * 投票请求处理
     *
     * @param voteRequest 请求
     * @return 返回
     */
    CompletableFuture<VoteResponse> handleVote(VoteRequest voteRequest);

}
