package com.lyon.dmeo.storage.client.raft.core;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public interface ResponseCode {

    int SUCCESS = 0;
    int LEADER_ALREADY_EXIST = 1;
    int TERM_EXPIRED = 2;
    int UN_COMMITTED_INDEX_LOW = 3;
//    int SUCCESS = 0;
//    int SUCCESS = 0;


}
