package com.lyon.dmeo.storage.client.raft.core.proccessor.core;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.protocol.api.remoting.Delegation;
import com.lyon.demo.protocol.api.remoting.RequestProcessor;
import com.lyon.demo.protocol.api.remoting.RequestProcessorDelegating;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@SuppressWarnings("all")
@SpiActivate(Protocols.NETTY)
public abstract class AbstractRequestProcessor<C, T> implements RequestProcessor<C, T>, RequestProcessorDelegating<T>, Delegation<RequestProcessorDelegating<T>> {
}
