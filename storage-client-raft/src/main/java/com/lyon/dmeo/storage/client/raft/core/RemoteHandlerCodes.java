package com.lyon.dmeo.storage.client.raft.core;

/**
 * 处理器code码表
 *
 * @author LeeYan9
 * @since 2022-05-10
 */
public interface RemoteHandlerCodes extends com.lyon.demo.protocol.api.core.RemoteHandlerCodes {


    /***
     * 心跳
     */
    int HEART_BEAT = 10;
    /**
     * 投票
     */
    int VOTE = 15;
    /**
     * 日志条目推送
     */
    int PUSH = 20;

}
