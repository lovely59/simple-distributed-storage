package com.lyon.dmeo.storage.client.raft.core.exception;

/**
 * @author LeeYan9
 * @since 2022-07-12
 */
public interface CommonException {

   String UNKNOWN = "未知错误";
   String DATA_NOT_NULL = "数据不能为空";
   String INCONSISTENT_STATE = "INCONSISTENT_STATE";

}
