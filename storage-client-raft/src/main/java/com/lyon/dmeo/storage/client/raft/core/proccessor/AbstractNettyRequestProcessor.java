package com.lyon.dmeo.storage.client.raft.core.proccessor;

import com.lyon.demo.protocol.netty.command.RemoteCommand;
import com.lyon.dmeo.storage.client.raft.core.proccessor.core.AbstractRequestProcessor;
import io.netty.channel.ChannelHandlerContext;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@SuppressWarnings({"rawtypes", "AlibabaClassNamingShouldBeCamel"})
public abstract class AbstractNettyRequestProcessor extends AbstractRequestProcessor<ChannelHandlerContext, RemoteCommand> {

    @Override
    public RemoteCommand processRequest(ChannelHandlerContext ctx, RemoteCommand remoteCommand) {

        RemoteCommand response = processRequest(remoteCommand);
        ctx.writeAndFlush(response);

        return response;
    }
}
