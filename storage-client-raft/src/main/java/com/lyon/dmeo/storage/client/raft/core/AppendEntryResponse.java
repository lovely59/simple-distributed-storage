package com.lyon.dmeo.storage.client.raft.core;

import com.lyon.demo.storage.client.api.core.command.request.RequestOrResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author LeeYan9
 * @since 2022-07-06
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AppendEntryResponse extends RequestOrResponse {

    private Code code;

    private long index;

    protected long endIndex;

    protected long beginIndex;

    protected long committedIndex;

    private int pos;


    public enum Code {

        /**
         * 消息追加结果状态
         */
        TERM_CHANGED,
        SUCCESS,
        TIMI_OUT,
        UNKNOWN_ERROR;
    }

}
