package com.lyon.dmeo.storage.client.raft.config;

import lombok.Data;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
public class RaftConfig {

    private String peers;

    private String selfId;

    private String group;

    private String storageFile;

}
