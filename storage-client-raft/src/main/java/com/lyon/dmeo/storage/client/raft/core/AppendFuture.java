package com.lyon.dmeo.storage.client.raft.core;

import cn.hutool.core.util.RandomUtil;
import com.lyon.demo.protocol.api.core.command.ResponseFuture;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
@SuppressWarnings("SingleStatementInBlock")
@Getter
@Setter
@EqualsAndHashCode
public class AppendFuture<R> extends ResponseFuture<R> {

    public AppendFuture() {
        super(RandomUtil.randomLong(32));
    }

    public AppendFuture(Long requestId, Long timeoutMillis) {
        super(requestId, timeoutMillis);
    }

    private int pos;

}
