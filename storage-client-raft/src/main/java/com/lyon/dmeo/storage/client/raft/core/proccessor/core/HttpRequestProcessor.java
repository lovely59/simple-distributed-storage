package com.lyon.dmeo.storage.client.raft.core.proccessor.core;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.protocol.api.remoting.RequestProcessorDelegating;
import com.lyon.demo.protocol.netty.command.RemoteCommand;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;
import com.lyon.dmeo.storage.client.raft.core.proccessor.AbstractNettyRequestProcessor;

import java.util.Collections;
import java.util.List;

/**
 * @author YLI1
 */
@SuppressWarnings({"rawtypes", "unused", "deprecation"})
@SpiActivate(Protocols.HTTP)
public class HttpRequestProcessor extends AbstractNettyRequestProcessor {

    private RequestProcessorDelegating<RemoteCommand> delegate;

    @Override
    public RemoteCommand processRequest(RemoteCommand remoteCommand) {
        if (delegate != null) {
            delegate.processRequest(remoteCommand);
        }
        return null;
    }

    @Override
    public List<Integer> handlerCodes() {
        if (delegate != null) {
            delegate.handlerCodes();
        }
        return Collections.emptyList();
    }

    @Override
    public void setDelegate(RequestProcessorDelegating<RemoteCommand> delegate) {
        this.delegate = delegate;
    }

    @Override
    public RequestProcessorDelegating<RemoteCommand> getDelegate() {
        return this.delegate;
    }
}