package com.lyon.demo.storage.client.api.core.core;

import cn.hutool.core.util.PrimitiveArrayUtil;
import cn.hutool.core.util.SerializeUtil;
import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author LeeYan9
 * @since 2022-07-07
 */
@Data
public class Entry implements Serializable {

    private long index;

    private int pos = -1;

    private String tag = "default";

    private byte[] body;

    public <T> T getData() {
        return PrimitiveArrayUtil.isEmpty(body) ? null : SerializeUtil.deserialize(body);
    }

    public String elegantToString() {
        return "Entry{" +
                "index=" + index +
                ", pos=" + pos +
                ", body=" + getData() +
                '}';
    }

    public boolean equalsTo(Entry entry) {
        if (this == entry) {
            return true;
        }
        if (entry == null || getClass() != entry.getClass()) {
            return false;
        }
        return index == entry.index && pos == entry.getPos() && Arrays.equals(body, entry.body);
    }

}
