package com.lyon.demo.storage.client.api.core.core;

import java.util.concurrent.CompletableFuture;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public interface StorageClient {


    /**
     * 保存数据
     *
     * @param data 数据
     * @return 结果
     */
    <T, R> CompletableFuture<R> commit(T data);

    /**
     * 添加k,v的日志条目 if key exists it cover
     *
     * @param key   键
     * @param value 值
     * @param <R>   返回值泛型
     * @param <K>   键泛型
     * @param <V>   值泛型
     * @return 结果
     */
    <R, K, V> CompletableFuture<R> commitEntry(K key, V value);

    /**
     * 添加k,v的日志条目 if key exists it cover
     *
     * @param tag   标签
     * @param key   键
     * @param value 值
     * @param <R>   返回值泛型
     * @param <K>   键泛型
     * @param <V>   值泛型
     * @return 结果
     */
    <R, K, V> CompletableFuture<R> commitEntry(String tag, K key, V value);

    /**
     * 获取数据
     *
     * @param index key数据
     * @return 结果
     */
    <R> CompletableFuture<R> get(long index);

    /**
     * 通过标签,键 获取日志条目
     *
     * @param tag 标签
     * @param key 键值
     * @param <R> 返回值泛型
     * @param <K> 键值泛型
     * @return 结果
     */
    <R, K> CompletableFuture<R> get(String tag, K key);


    /**
     * 通过键 获取日志条目
     *
     * @param key 键值
     * @param <R> 返回值泛型
     * @param <K> 键值泛型
     * @return 结果
     */
    <R, K> CompletableFuture<R> get(K key);

}
