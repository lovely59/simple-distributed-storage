package com.lyon.demo.storage.client.api.core.command.response;

import com.lyon.demo.storage.client.api.core.command.request.RequestOrResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class VoteResponse extends RequestOrResponse {

    protected long leaderTerm;

    protected long ledgerIndex;

    protected VoteState voteState;

    public enum VoteState {
        /**
         * 投票结果
         */
        UNKNOWN,
        ACCEPT,
        REJECT_UNEXPECTED_LEADER,
        REJECT_UNKNOWN_LEADER,
        REJECT_ALREADY_HAS_LEADER,
        REJECT_ALREADY_VOTED,
        REJECT_TERM_EXPIRED,
        REJECT_EXPIRED_LEDGER_TERM,
        REJECT_TERM_NOT_READY,
        REJECT_TERM_SMALL_THAN_LEDGER,
        REJECT_SMALL_LEDGER_END_INDEX,
        REJECT_TAKING_LEADERSHIP,
    }

    public enum VoteNextType {
        /**
         * 下次投票类型
         */
        WAIT_TO_REVOTE,
        REVOTE_IMMEDIATELY,
        PASSED,
        WAIT_TO_VOTE_NEXT;
    }

}
