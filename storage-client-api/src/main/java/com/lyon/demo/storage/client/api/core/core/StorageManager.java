package com.lyon.demo.storage.client.api.core.core;

import com.lyon.demo.storage.common.thread.StartupShutdownAble;

import java.util.concurrent.CompletableFuture;

/**
 * @author LeeYan9
 * @since 2022-07-15
 */
public interface StorageManager<K, V> extends StartupShutdownAble {

    /**
     * 保存数据
     *
     * @param data 数据
     * @return 结果
     */
    <R extends Entry> CompletableFuture<R> commit(K data);

    /**
     * 添加k,v的日志条目 if key exists it cover
     *
     * @param key   键
     * @param value 值
     * @return 结果
     */
    <R extends Entry> CompletableFuture<R> commitEntry(K key, V value);

    /**
     * 添加k,v的日志条目 if key exists it cover
     *
     * @param tag   标签
     * @param key   键
     * @param value 值
     * @return 结果
     */
    <R extends Entry> CompletableFuture<R> commitEntry(String tag, K key, V value);

    /**
     * 获取数据
     *
     * @param index key数据
     * @return 结果
     */
    <R> CompletableFuture<R> get(long index);

    /**
     * 通过标签,键 获取日志条目
     *
     * @param tag 标签
     * @param key 键值
     * @return 结果
     */
    <R extends Entry> CompletableFuture<R> get(String tag, K key);


    /**
     * 通过键 获取日志条目
     *
     * @param key 键值
     * @return 结果
     */
    <R extends Entry> CompletableFuture<R> get(K key);

}
