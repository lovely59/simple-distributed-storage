package com.lyon.demo.storage.client.api.core.protocol.core;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
public interface RoleChangeHandler {

    /**
     * 转换角色
     * @param role 角色类型
     */
    void changeRole(Role role);

}
