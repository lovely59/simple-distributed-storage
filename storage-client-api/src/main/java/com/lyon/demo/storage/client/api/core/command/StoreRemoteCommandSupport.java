package com.lyon.demo.storage.client.api.core.command;

import com.lyon.demo.protocol.api.core.CommandSupport;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public class StoreRemoteCommandSupport<T> implements CommandSupport {

    @Override
    public long getRequestId() {
        return 0;
    }
}
