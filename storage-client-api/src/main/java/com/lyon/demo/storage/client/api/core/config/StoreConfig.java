package com.lyon.demo.storage.client.api.core.config;

import lombok.Data;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@Data
public class StoreConfig {

    private String storeProtocol;

}
