package com.lyon.demo.storage.client.api.core.command.response;

import com.lyon.demo.storage.client.api.core.command.request.RequestOrResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HeartBeatResponse extends RequestOrResponse {

    private ResponseCode responseCode;

    public enum ResponseCode {

        /**
         * 心跳结果
         */
        // 成功(term相等,leaderId相等)
        SUCCESS,
        // leader term 小于 peer term
        TERM_EXPIRED,
        // leader term 大于 peer term
        TERM_NOT_READY,
        // leader id <> peer leader 发生了分区，并且term一样。 理论上不会发生
        INCONSISTENT_LEADER,
        NONEXISTENT_PEER,
        // 网络异常/发生错误
        UNKNOWN_ERROR;

    }

}
