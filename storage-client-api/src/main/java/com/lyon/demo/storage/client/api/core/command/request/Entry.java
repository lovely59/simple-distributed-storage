package com.lyon.demo.storage.client.api.core.command.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LeeYan9
 * @since 2022-07-07
 */
@Data
public class Entry implements Serializable {

    private byte[] body;

    private long index;

    private long pos;


}
