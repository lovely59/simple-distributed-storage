package com.lyon.demo.storage.client.api.core.command.response;

import com.lyon.demo.storage.client.api.core.core.Entry;
import com.lyon.demo.storage.client.api.core.command.request.RequestOrResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PushEntryResponse extends RequestOrResponse {

    //todo


    public PushEntryResponse(PushResponseType type) {
        this.type = type;
    }

    protected long index;

    protected long endIndex;

    protected long beginIndex;

    protected long committedIndex;

    protected PushResponseType type;

    private Entry entry;

    public enum PushResponseType {
        SUCCESS,
        UNKNOWN,
        INCONSISTENT_STATE
    }

}
