package com.lyon.demo.storage.client.api.core.protocol.core;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */

public enum Role {
    /**
     * 节点角色
     */
    UNKNOWN,
    FOLLOWER,
    CANDIDATE,
    LEADER,
    ;

}

