package com.lyon.demo.storage.client.api.core.protocol.storage;

/**
 * @author LeeYan9
 * @since 2022-07-13
 */
@SuppressWarnings("all")
public interface Strategies {

    String MEMORY = "MEMORY";
    String M_MAP_FILE = "M_MAP_FILE";

}
