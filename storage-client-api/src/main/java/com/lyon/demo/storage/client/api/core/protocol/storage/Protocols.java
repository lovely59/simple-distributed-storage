package com.lyon.demo.storage.client.api.core.protocol.storage;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
public interface Protocols {

    String RAFT = "RAFT";

}
