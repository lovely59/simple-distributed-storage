package com.lyon.demo.storage.client.api.core.core;

import com.lyon.demo.storage.client.api.core.protocol.storage.Strategies;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;
import lombok.Getter;
import lombok.Setter;

/**
 * @author LeeYan9
 * @since 2022-07-06
 */
@Getter
@Setter
@SpiActivate(Strategies.MEMORY)
public abstract class AbstractStoreService {

    protected volatile long beginIndex = -1;
    protected volatile long endIndex = -1;
    protected volatile long committedIndex = -1;

    protected MemberState memberState;

    /**
     * 更新已提交偏移位
     *
     * @param currTerm 当前任期
     * @param index    索引
     */
    public abstract void updateCommittedIndex(long currTerm, Long index);

    /**
     * 获取索引对应数据
     *
     * @param index 索引
     * @return 数据
     */
    public abstract Entry get(long index);

    /**
     * 数据截断：（小于endIndex 截断多余的部分）
     * @param term 任期编号
     * @param leaderId leaderId
     * @param entry 数据
     */
    public abstract long truncate(Entry entry, long term, String leaderId);

    /**
     * 来自follower节点的数据追加
     *
     * @param entry 数据
     * @return 结果索引
     */
    public abstract long appendAsFollower(Entry entry);

    /**
     * 来自Leader节点的数据追加
     *
     * @param entry 数据
     * @return 结果索引
     */
    public abstract void appendAsLeader(Entry entry);

    /**
     * 拿到已经提交的数据
     *
     * @param index 索引
     * @return 数据
     */
    public abstract Entry hitEntry(long index);

    /**
     * 拿到已经提交的数据
     *
     * @param tag 标签
     * @param key 键
     * @return 数据
     */
    public abstract <K> Entry hitEntry(String tag, K key);
}
