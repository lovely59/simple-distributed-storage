package com.lyon.demo.storage.client.api.core.config;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.protocol.api.transport.config.RemoteClientConfig;
import com.lyon.demo.storage.client.api.core.protocol.storage.Strategies;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
@Data
@Accessors(chain = true)
public class DLedgerConfig extends StoreConfig {

    private RemoteClientConfig remoteClientConfig;
    private RemoteClientConfig remoteServerConfig;

    private long connectTimeout = 3000;
    private long readTimeout = 3000;
    /**
     * 心跳结果等待超时时间
     */
    private long heartBeatTimeout = 3000;
    private long voteResponseTimeout = 3000;

    /**
     * 心跳时间间隔
     */
    private long heartBeatTimeInterval = 2000;

    private int maxHeartBeatLeak = 2;

    private int maxVoteIntervalMs = 1000;
    private int minVoteIntervalMs = 300;

    private long maxAckTimeMs = 1000;

    private String remoteProtocol = Protocols.NETTY;
    private String storeStrategy = Strategies.MEMORY;


    private String peers;

    private String selfId;

    private String group;

    private String storageFile = System.getenv("user.dir") + "/storage/raft/";

}
