package com.lyon.demo.storage.client.api.core.exception;

/**
 * @author LeeYan9
 * @since 2022-07-14
 */
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public class DLedgerException extends RuntimeException{

    public DLedgerException(String message) {
        super(message);
    }
}
