package com.lyon.demo.storage.client.api.core.command.request;

import com.lyon.demo.storage.client.api.core.core.Entry;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PushEntryRequest extends RequestOrResponse {

    protected long index;

    protected long endIndex;

    protected long beginIndex;

    protected long committedIndex;

    protected Type type;

    protected Entry entry;

    /**
     * 消息推送类型
     * 出现commitOffset差距过大时,比较完成后,才能截断
     */
    public enum Type {
        // 追加数据
        APPEND,
        // 提交offset
        COMMIT,
        // 比较
        COMPARE,
        // 截断
        TRUNCATE
    }

}
