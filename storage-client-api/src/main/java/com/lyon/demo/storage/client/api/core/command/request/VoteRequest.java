package com.lyon.demo.storage.client.api.core.command.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VoteRequest extends RequestOrResponse {

    protected long leaderEndTerm;

    protected long ledgerEndIndex;

}
