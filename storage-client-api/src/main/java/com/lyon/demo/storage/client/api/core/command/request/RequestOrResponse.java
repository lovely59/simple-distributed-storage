package com.lyon.demo.storage.client.api.core.command.request;

import cn.hutool.core.text.StrFormatter;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@SuppressWarnings("AlibabaAbstractClassShouldStartWithAbstractNaming")
@Getter
@Setter
public abstract class RequestOrResponse implements Serializable {

    protected String group;

    protected Long term;

    protected String leaderId;

    protected String localId;

    protected String remoteId;

    public String baseInfo() {
        return StrFormatter.format("group:[{}] term:[{}] leaderId:[{}]", group, term, leaderId);
    }
}
