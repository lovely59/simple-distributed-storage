package com.lyon.demo.storage.client.api.core.protocol.core;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface RoleChangeSupport {

    /**
     * 添加角色变更处理器
     * @param handler 处理器
     */
    void addRoleChangeHandler(RoleChangeHandler handler);
}
