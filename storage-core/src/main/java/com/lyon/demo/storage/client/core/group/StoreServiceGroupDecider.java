package com.lyon.demo.storage.client.core.group;

import com.lyon.demo.storage.client.core.consistency.ConsistencyService;

import java.util.List;

/**
 * 支持数据存储 group 后期支持一致性hash？
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface StoreServiceGroupDecider<T> {

    void init(List<ConsistencyService> services);

    /**
     * 确定存储哪个group
     * @param data 数据
     * @return 实际存储组
     */
    String determine(T data);

}
