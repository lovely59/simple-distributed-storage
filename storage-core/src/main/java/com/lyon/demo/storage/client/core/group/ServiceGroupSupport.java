package com.lyon.demo.storage.client.core.group;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface ServiceGroupSupport {

    /**
     * 当前所属组
     *
     * @return 组名
     */
    ServiceGroup group();

}
