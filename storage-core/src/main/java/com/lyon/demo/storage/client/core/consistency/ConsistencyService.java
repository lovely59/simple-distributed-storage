package com.lyon.demo.storage.client.core.consistency;

import com.lyon.demo.storage.client.api.core.core.StorageManager;
import com.lyon.demo.storage.client.api.core.protocol.storage.Protocols;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@SuppressWarnings("all")
@SpiActivate(Protocols.RAFT)
public interface ConsistencyService<K, V, Config> extends StorageManager<K, V> {


    /**
     * 参数准备
     *
     * @param config 配置
     */
    void prepare(Config config);
}
