package com.lyon.demo.storage.client.core.consistency;

import com.lyon.demo.storage.client.api.core.config.DLedgerConfig;
import com.lyon.demo.storage.client.api.core.core.Entry;
import com.lyon.demo.storage.client.api.core.protocol.storage.Protocols;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;
import com.lyon.dmeo.storage.client.raft.DLedgerServer;

import java.util.concurrent.CompletableFuture;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@SuppressWarnings({"unused", "AlibabaServiceOrDaoClassShouldEndWithImpl"})
@SpiActivate(Protocols.RAFT)
public class RaftStoreConsistencyService<K, V> implements ConsistencyService<K, V, DLedgerConfig> {

    private DLedgerServer dLedgerServer;

    @Override
    public void prepare(DLedgerConfig dLedgerConfig) {
        this.dLedgerServer = new DLedgerServer(dLedgerConfig);
    }

    @Override
    public void shutdown() {
        dLedgerServer.shutdown();
    }

    @Override
    public void startup() {
        dLedgerServer.startup();
    }

    @Override
    public <R extends Entry> CompletableFuture<R> commit(K data) {
        return dLedgerServer.commit(data);
    }

    @Override
    public <R extends Entry> CompletableFuture<R> commitEntry(K key, V value) {
        return dLedgerServer.commitEntry(key, value);
    }

    @Override
    public <R extends Entry> CompletableFuture<R> commitEntry(String tag, K key, V value) {
        return dLedgerServer.commitEntry(tag, key, value);
    }

    @Override
    public <R> CompletableFuture<R> get(long index) {
        return dLedgerServer.get(index);
    }

    @Override
    public <R extends Entry> CompletableFuture<R> get(String tag, K key) {
        return dLedgerServer.get(tag, key);
    }

    @Override
    public <R extends Entry> CompletableFuture<R> get(K key) {
        return dLedgerServer.get(key);
    }
}
