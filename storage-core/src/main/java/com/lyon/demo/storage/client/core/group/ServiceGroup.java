package com.lyon.demo.storage.client.core.group;

import lombok.Data;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
@Data
public class ServiceGroup {

    private String address;

    private String group;

}
