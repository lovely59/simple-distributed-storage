package com.lyon.demo.test.storage.memory;

import cn.hutool.core.lang.Pair;
import cn.hutool.core.text.StrFormatter;
import com.lyon.demo.protocol.netty.config.NettyRemoteServerConfig;
import com.lyon.demo.storage.client.api.core.config.DLedgerConfig;
import com.lyon.demo.storage.client.api.core.core.Entry;
import com.lyon.demo.storage.client.api.core.protocol.storage.Strategies;
import com.lyon.dmeo.storage.client.raft.DLedgerServer;
import lombok.SneakyThrows;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author LeeYan9
 * @since 2022-06-15
 */
public abstract class DLedgerBaseUnitTest {

    // peers
    protected String peers = "n0-localhost:8091;n1-localhost:8092;n2-localhost:8093";
    // group
    protected String group = "group0";

    public DLedgerServer parseServers(ArrayList<DLedgerServer> servers, AtomicInteger leaderNum, AtomicInteger followerNum) {
        DLedgerServer leaderServer = null;
        for (DLedgerServer server : servers) {
            if (server.getMemberState().isLeader()) {
                leaderNum.incrementAndGet();
                leaderServer = server;
            } else if (server.getMemberState().isFollower()) {
                followerNum.incrementAndGet();
            }
        }
        return leaderServer;
    }

    public DLedgerServer launchServer(String group, String peers, String selfId) {
        DLedgerServer dLedgerServer = new DLedgerServer(buildOfDefaultDLedgerConfig(group, peers, selfId));
        dLedgerServer.startup();
        return dLedgerServer;
    }

    public DLedgerConfig buildOfDefaultDLedgerConfig(String group, String peers, String selfId) {
        return new DLedgerConfig()
                .setPeers(peers)
                .setGroup(group)
                .setSelfId(selfId)
                .setStoreStrategy(Strategies.MEMORY)
                .setRemoteServerConfig(NettyRemoteServerConfig.ofDefault());
    }

    @SneakyThrows
    public <T> Entry commit(DLedgerServer leaderServer, T data) {
        CompletableFuture<Entry> future = leaderServer.commit(data);
        Entry entry = future.get();
        System.err.println("提交日志完成，返回结果： " + entry.elegantToString());
        Assert.assertNotNull(entry);
        Assert.assertEquals(data, entry.getData());

        CompletableFuture<Entry> futureGet = leaderServer.get(entry.getIndex());
        Entry entryGet = futureGet.get();
        Assert.assertNotNull(entryGet);
        Assert.assertEquals(data, entryGet.getData());
        System.err.println("通过index拿到日志记录： " + entryGet.elegantToString());

        return entryGet;
    }

    @SneakyThrows
    public <K, V> Entry commitEntry(DLedgerServer leaderServer, String tag, K key, V value) {
        CompletableFuture<Entry> future2 = leaderServer.commitEntry(tag, key, value);
        Entry entry2 = future2.get();
        Assert.assertNotNull(entry2);
        Pair<K, V> pair = new Pair<>(key, value);
        Assert.assertEquals(pair, entry2.getData());
        System.err.println(StrFormatter.format("提交entry-key[{}] 日志完成，返回结果： {}", key, entry2.elegantToString()));


        CompletableFuture<Entry> future2Get = leaderServer.get(tag, key);
        Entry entry2Get = future2Get.get();
        Assert.assertNotNull(entry2Get);
        Assert.assertEquals(pair, entry2Get.getData());
        System.err.println(StrFormatter.format("通过entry-key[{}] 拿到日志记录：{} ", key, entry2Get.elegantToString()));

        return entry2Get;
    }


}
