package com.lyon.demo.protocol.netty;

import com.lyon.demo.protocol.api.core.RemoteHandlerCodes;

/**
 * @author LeeYan9
 * @since 2022-05-25
 */
public interface ExampleHandlerCodes extends RemoteHandlerCodes {

    int HEART_BEAT = 10;

}
