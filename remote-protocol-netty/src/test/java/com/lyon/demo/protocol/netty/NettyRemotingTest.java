package com.lyon.demo.protocol.netty;

import cn.hutool.core.util.SerializeUtil;
import cn.hutool.json.JSONUtil;
import com.lyon.demo.protocol.api.core.command.RemotingCommandType;
import com.lyon.demo.protocol.api.core.command.ResponseFuture;
import com.lyon.demo.protocol.api.remoting.AbstractRemotingService;
import com.lyon.demo.protocol.api.remoting.RequestProcessor;
import com.lyon.demo.protocol.api.transport.Transport;
import com.lyon.demo.protocol.netty.client.NettyRemoteClient;
import com.lyon.demo.protocol.netty.command.CommonRemoteCommand;
import com.lyon.demo.protocol.netty.command.RemoteCommand;
import com.lyon.demo.storage.common.SystemClock;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;

import java.io.Serializable;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

/**
 * @author LeeYan9
 * @since 2022-05-25
 */
@SuppressWarnings({"rawtypes", "unchecked", "ResultOfMethodCallIgnored"})
@Slf4j
public class NettyRemotingTest extends NettyRemotingBaseUnitTest {


    private final BiConsumer<RemoteCommand<HeartBeatResponse>, Throwable> heartbeat_consumer = (command, throwable) -> {
        if (throwable != null) {
            log.error("请求失败", throwable);
        } else {
            Object data = SerializeUtil.deserialize(command.getPayload());
            log.info("同步结果回执{}-{}", command.getRequestId(), data);
        }
    };

    @Test
    public void test_SyncSend() {
        InetSocketAddress serverAddress = new InetSocketAddress(8080);
        // 添加服务端的心跳处理器
        addHeartBeatProcessor(createNettyServer(serverAddress.getPort()));
        NettyRemoteClient nettyRemoteClient = createNettyClient();
        // 创建和服务端的连接通道
        Transport transport = nettyRemoteClient.createIfAbsent(serverAddress, Duration.ofMillis(3000));
        // 准备请求命令1
        byte[] payload = SerializeUtil.serialize(new HeartBeatRequest());
        RemoteCommand<?> command1 = new CommonRemoteCommand<>(RemotingCommandType.REQUEST, ExampleHandlerCodes.HEART_BEAT, payload);
        // 准备请求命令2
        byte[] payload2 = SerializeUtil.serialize(new HeartBeatRequest());
        RemoteCommand<HeartBeatRequest> command2 = new CommonRemoteCommand<>(RemotingCommandType.REQUEST, ExampleHandlerCodes.HEART_BEAT, payload2);
        // 发送请求
        ResponseFuture<RemoteCommand<HeartBeatResponse>> future1 = syncSend(transport, command1, heartbeat_consumer);
        ResponseFuture<RemoteCommand<HeartBeatResponse>> future2 = syncSend(transport, command2, heartbeat_consumer);
        Assert.assertNotNull(future1);
        Assert.assertNotNull(future2);
        ResponseFuture.join(future1, future2);
    }

    private <R> ResponseFuture<RemoteCommand<R>> syncSend(Transport transport, RemoteCommand<?> command, BiConsumer<RemoteCommand<R>, Throwable> consumer) {
        // 发送同步请求
        ResponseFuture<RemoteCommand<R>> future = transport.send(command, null);
        // 测试同步请求回调
        return future.whenComplete(consumer);
    }

    @SneakyThrows
    @Test
    public void test_AllSend() {
        InetSocketAddress serverAddress = new InetSocketAddress(8081);
        // 添加服务端的心跳处理器
        addHeartBeatProcessor(createNettyServer(serverAddress.getPort()));
        NettyRemoteClient nettyRemoteClient = createNettyClient();
        // 创建和服务端的连接通道
        Transport<RemoteCommand, RemoteCommand<HeartBeatResponse>> transport = nettyRemoteClient.createIfAbsent(serverAddress, Duration.ofMillis(3000));
        // 准备请求命令
        byte[] payload = SerializeUtil.serialize(new HeartBeatRequest());
        RemoteCommand<HeartBeatRequest> command = new CommonRemoteCommand<>(RemotingCommandType.REQUEST, ExampleHandlerCodes.HEART_BEAT, payload);
        ResponseFuture<RemoteCommand<HeartBeatResponse>> future = syncSend(transport, command, heartbeat_consumer);

        // 发送异步请求
        CountDownLatch countDownLatch = new CountDownLatch(1);
        RemoteCommand<HeartBeatRequest> command2 = new CommonRemoteCommand(RemotingCommandType.REQUEST, ExampleHandlerCodes.HEART_BEAT, payload);
        transport.asyncSend(command2, null, responseFuture -> {
            Assert.assertNotNull(responseFuture);
            try {
                responseFuture.whenComplete((respCommand, ex) -> {
                    if (ex == null) {
                        HeartBeatResponse heartBeatResponse = respCommand.parseData();
                        log.info("异步结果回执：" + heartBeatResponse.getSendTimeMillis());
                    } else {
                        log.error("异步结果回执失败", ex);
                    }
                });
            } finally {
                countDownLatch.countDown();

            }

        });
        ResponseFuture.join(future);
        countDownLatch.await(3, TimeUnit.SECONDS);

    }


    private void addHeartBeatProcessor(AbstractRemotingService remotingService) {

        remotingService.addRequestProcessor(new RequestProcessor<ChannelHandlerContext, RemoteCommand>() {
            @Override
            public RemoteCommand processRequest(ChannelHandlerContext ctx, RemoteCommand command) {
                log.info("接收到{}-{}心跳请求[{}]", ctx.channel().remoteAddress(), command.getRequestId(), command.parseData());
                RemoteCommand<HeartBeatResponse> response = CommonRemoteCommand.ofResponse(command, new HeartBeatResponse());
                ctx.writeAndFlush(response);
                return response;
            }

            @Override
            public List<Integer> handlerCodes() {
                return List.of(ExampleHandlerCodes.HEART_BEAT);
            }
        }, null);
    }


    @Data
    static class HeartBeatRequest implements Serializable {
        private long sendTimeMillis = SystemClock.now();

        private String name1 = "心跳请求";

    }

    @Data
    static class HeartBeatResponse implements Serializable {
        private long sendTimeMillis = SystemClock.now();

        private String name2 = "心跳结果";
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testSwitch() {
        int a = 2;
        int b;
        switch (a) {
            case 2:
            case 3:
                b = 3;
                break;
            default:
                b = 4;
                break;
        }
        System.out.println(b);
    }

    @SneakyThrows
    @Test
    public void test_AsyncSend() {
        InetSocketAddress serverAddress = new InetSocketAddress(8082);
        // 添加服务端的心跳处理器
        addHeartBeatProcessor(createNettyServer(serverAddress.getPort()));
        NettyRemoteClient nettyRemoteClient = createNettyClient();
        // 创建和服务端的连接通道
        Transport<RemoteCommand, RemoteCommand> transport = nettyRemoteClient.createIfAbsent(serverAddress, Duration.ofMillis(3000));
        // 准备请求命令1
        byte[] payload = SerializeUtil.serialize(new HeartBeatRequest());
        RemoteCommand<?> command1 = new CommonRemoteCommand<>(RemotingCommandType.REQUEST, ExampleHandlerCodes.HEART_BEAT, payload);
        CompletableFuture future = new CompletableFuture();
        transport.asyncSend(command1, null, responseFuture -> {
            if (responseFuture.getCause() == null) {
                future.complete(responseFuture.getData());
            } else {
                future.completeExceptionally(responseFuture.getCause());
            }
        });
        Object o = future.get();
        System.out.println(JSONUtil.toJsonStr(o));
    }

}
