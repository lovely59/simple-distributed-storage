package com.lyon.demo.protocol.netty;

import com.lyon.demo.protocol.netty.client.NettyRemoteClient;
import com.lyon.demo.protocol.netty.config.NettyRemoteServerConfig;
import com.lyon.demo.protocol.netty.server.NettyRemoteServer;

import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-06-15
 */
@SuppressWarnings("rawtypes")
public class NettyRemotingBaseUnitTest {

    protected NettyRemoteServer createNettyServer(int port) {
        // 创建Netty服务端 默认端口8080
        NettyRemoteServerConfig remoteServerConfig = NettyRemoteServerConfig.ofDefault();
        remoteServerConfig.setListenPort(port);
        NettyRemoteServer nettyRemoteServer = new NettyRemoteServer<>();
        // 启动服务
        Properties prop = new Properties();
        prop.put(NettyRemoteServer.SERVER_CONFIG_kEY,remoteServerConfig);
        nettyRemoteServer.prepare(prop);
        nettyRemoteServer.start();
        return nettyRemoteServer;
    }

    NettyRemoteClient createNettyClient() {
        // 创建Netty客户端
        NettyRemoteClient nettyRemoteClient = new NettyRemoteClient();
        nettyRemoteClient.prepare(null);
        nettyRemoteClient.start();
        return nettyRemoteClient;
    }

}
