package com.lyon.demo.protocol.netty.config;

import com.lyon.demo.protocol.api.transport.config.RemoteClientConfig;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
@Data
@Accessors(chain = true)
public class NettyRemoteServerConfig extends RemoteClientConfig {

    private int serverSocketSndBufSize = 10000;
    private int serverSocketRcvBufSize = 10000;
    private int connectTimeoutMillis = 5000;
    private int serverIdlTimeSeconds = 10;

    public static NettyRemoteServerConfig ofDefault() {
        NettyRemoteServerConfig configRemote = new NettyRemoteServerConfig();
        configRemote.setListenPort(8080);
        return configRemote;
    }

}
