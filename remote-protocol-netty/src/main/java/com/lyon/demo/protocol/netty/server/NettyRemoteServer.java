package com.lyon.demo.protocol.netty.server;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.protocol.api.core.Command;
import com.lyon.demo.protocol.api.remoting.RemotingRequestHandler;
import com.lyon.demo.protocol.api.transport.TransportServer;
import com.lyon.demo.protocol.netty.config.NettyRemoteServerConfig;
import com.lyon.demo.protocol.netty.core.AbstractNettyRemotingService;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
@SuppressWarnings({"rawtypes", "noinspection", "FieldCanBeLocal"})
@SpiActivate(Protocols.NETTY)
@Slf4j
public class NettyRemoteServer<E, T extends Command<E>> extends AbstractNettyRemotingService<T> implements TransportServer<ChannelHandlerContext, T, NettyRemoteServerConfig> {

    private ServerBootstrap serverBootstrap;
    private NettyRemoteServerConfig nettyRemoteServerConfig;
    private RemotingRequestHandler remotingRequestHandler;
    private SocketAddress localAddress;
    public static final String SERVER_CONFIG_kEY = "netty-server-config";

    @Override
    public void prepare(Properties properties) {
        if (this.nettyRemoteServerConfig == null) {
            this.nettyRemoteServerConfig = properties == null ? null : (NettyRemoteServerConfig) properties.get(SERVER_CONFIG_kEY);
        }
        if (nettyRemoteServerConfig == null) {
            this.nettyRemoteServerConfig = NettyRemoteServerConfig.ofDefault();
        }
        if (nettyRemoteServerConfig.getListenPort() > 0) {
            this.localAddress = new InetSocketAddress(nettyRemoteServerConfig.getListenPort());
        }
        prepareSharableInstances();
    }

    private void prepareSharableInstances() {
        this.remotingRequestHandler = new NettyRemotingRequestHandler();
    }

    @SneakyThrows
    @Override
    public void start() {
        this.serverBootstrap = new ServerBootstrap();
        NioEventLoopGroup boosGroup = new NioEventLoopGroup(1);
        NioEventLoopGroup workGroup = new NioEventLoopGroup(Runtime.getRuntime().availableProcessors());

        serverBootstrap.group(boosGroup, workGroup)
                .channel(NioServerSocketChannel.class)
                .localAddress(localAddress)
                .option(ChannelOption.SO_BACKLOG, 1024)
                .option(ChannelOption.SO_REUSEADDR, true)
                .option(ChannelOption.SO_KEEPALIVE, false)
                .childOption(ChannelOption.TCP_NODELAY, true)
                .childOption(ChannelOption.SO_SNDBUF, nettyRemoteServerConfig.getServerSocketSndBufSize())
                .childOption(ChannelOption.SO_RCVBUF, nettyRemoteServerConfig.getServerSocketRcvBufSize())
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) {
                        //noinspection rawtypes
                        socketChannel
                                .pipeline()
                                .addLast(new ObjectEncoder())
                                .addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(getClass().getClassLoader())))
                                .addLast(new IdleStateHandler(0, 0, nettyRemoteServerConfig.getServerIdlTimeSeconds()))
                                .addLast((SimpleChannelInboundHandler) remotingRequestHandler);
                    }
                });
        serverBootstrap
                .bind()
                .sync()
                .addListener(future -> {
                    if (future.isSuccess()) {
                        log.info("Netty-Server started..[{}]",localAddress);
                    } else {
                        throw new IllegalStateException("启动服务失败");
                    }
                });
    }

    @Override
    public void setConfig(NettyRemoteServerConfig nettyRemoteServerConfig) {
        this.nettyRemoteServerConfig = nettyRemoteServerConfig;
    }

    @Override
    public void shutdown() {
        this.serverBootstrap = null;
    }

    @ChannelHandler.Sharable
    class NettyRemotingRequestHandler extends SimpleChannelInboundHandler<T> implements RemotingRequestHandler<ChannelHandlerContext, T> {

        @Override
        public void processRequest(ChannelHandlerContext ctx, T command) {
            NettyRemoteServer.this.processRequest(ctx, command);
        }

        @Override
        protected void messageReceived(ChannelHandlerContext ctx, T command) {
            processRequest(ctx, command);
        }
    }

}
