package com.lyon.demo.protocol.netty.command;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author YLI1
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CommonHeader extends Header {

    public CommonHeader(int handlerCode, byte type) {
        this.handlerCode = handlerCode;
        this.type = type;
    }

    @Override
    public int length() {
        return 0;
    }
}