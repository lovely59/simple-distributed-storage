package com.lyon.demo.protocol.netty.command;

import com.lyon.demo.protocol.api.core.RequestStatus;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@Getter
@Setter
public abstract class Header implements Serializable {

    /**
     * 请求业务类型
     */
    protected int handlerCode;

    /**
     * 请求结果状态
     */
    protected int status = RequestStatus.SUCCESS;

    /**
     * 请求类型
     */
    protected byte type;


    private static final AtomicLong REQUEST_ID = new AtomicLong(0);

    /**
     * 请求id
     */
    protected long requestId = REQUEST_ID.incrementAndGet();

    public Header() {
    }

    public Header(int handlerCode, byte type) {
        this.handlerCode = handlerCode;
        this.type = type;
    }

    /**
     * 头长度
     *
     * @return 头长度
     */
    public abstract int length();
}
