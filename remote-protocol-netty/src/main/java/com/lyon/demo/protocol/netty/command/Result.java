package com.lyon.demo.protocol.netty.command;

import cn.hutool.core.lang.Assert;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Lyon
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Result<T> implements Serializable {

    public static final int SUCCESS = 0;
    private static int FAILURE = -1;

    private int code;
    private String message;
    private T data;

    public static Result success(){
        return new Result(SUCCESS,null,null);
    }

    public static <T> Result success(T data){
        return new Result(SUCCESS,null,data);
    }

    public static Result failure(String message){
        return new Result(FAILURE,message,null);
    }

    public static void checkSuccess(Result result){
        Assert.notNull(result);
        Assert.state(result.code == SUCCESS);
    }

}
