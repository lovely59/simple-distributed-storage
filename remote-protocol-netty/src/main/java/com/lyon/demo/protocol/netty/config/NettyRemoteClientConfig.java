package com.lyon.demo.protocol.netty.config;

import com.lyon.demo.protocol.api.transport.config.RemoteClientConfig;
import lombok.Data;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
@Data
public class NettyRemoteClientConfig extends RemoteClientConfig {

    private int serverSocketSndBufSize = 10000;
    private int serverSocketRcvBufSize = 10000;
    private int connectTimeoutMillis = 5000;
    private int serverIdlTimeSeconds = 10;


    public static NettyRemoteClientConfig ofDefault() {
        return new NettyRemoteClientConfig();
    }

}
