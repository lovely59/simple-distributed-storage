package com.lyon.demo.protocol.api.transport;

import com.lyon.demo.protocol.api.core.Command;
import com.lyon.demo.protocol.api.core.RemotingCallback;
import com.lyon.demo.protocol.api.core.command.ResponseFuture;

import java.time.Duration;

/**
 * @param <T> 请求泛型
 * @param <R> 结果泛型
 * @author LeeYan9
 * @since 2022-05-10
 */
@SuppressWarnings("rawtypes")
public interface Transport<T extends Command, R extends Command> {

    /**
     * 发生请求到远端
     *
     * @param command 请求
     * @param timeout 超时时间
     * @return 结果
     */
    ResponseFuture<R> send(T command, Duration timeout);

    /**
     * 异步发生请求到远端
     *
     * @param command          请求
     * @param remotingCallback 回调
     * @param timeout          超时时间
     */
    void asyncSend(T command, Duration timeout, RemotingCallback<ResponseFuture<R>> remotingCallback);

    /**
     * 异步发生请求到远端
     *
     * @param command          请求
     * @param remotingCallback 回调
     */
    void asyncSend(T command, RemotingCallback<ResponseFuture<R>> remotingCallback);
}
