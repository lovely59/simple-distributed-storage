package com.lyon.demo.protocol.api.remoting;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
public interface RemotingRequestHandler<CTX, T> {

    /**
     * 执行请求处理
     * @param ctx 上下文
     * @param command 命令
     * @throws Exception 异常
     */
    void processRequest(CTX ctx, T command) throws Exception;
}
