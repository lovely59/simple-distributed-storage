package com.lyon.demo.protocol.api.core;

/**
 * @author LeeYan9
 * @since 2022-05-25
 */
public interface RequestHandlerCode {

    /**
     * 处理类型code
     *
     * @return 处理类型code
     */
    int handlerCode();

}
