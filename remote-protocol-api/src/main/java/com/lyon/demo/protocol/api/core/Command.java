package com.lyon.demo.protocol.api.core;

import cn.hutool.core.clone.Cloneable;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
public interface Command<T> extends Cloneable<T>, CommandSupport, RequestHandlerCode {
    /**
     * 请求类型（0：来自对端服务的请求；1：来自对端服务的返回）
     *
     * @return 请求类型
     */
    byte getCommandType();

    /**
     * 请求类型（0：来自对端服务的请求；1：来自对端服务的返回）
     *
     * @param type 请求类型
     */
    void setCommandType(byte type);
}
