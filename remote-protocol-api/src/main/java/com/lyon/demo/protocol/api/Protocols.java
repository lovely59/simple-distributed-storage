package com.lyon.demo.protocol.api;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public interface Protocols {

    String NETTY = "NETTY";

    @Deprecated
    String HTTP = "HTTP";
}
