package com.lyon.demo.protocol.api.core;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface CommandSupport {

    /**
     * 请求id
     *
     * @return 请求类型
     */
    long getRequestId();

    /**
     * 处理状态code: [0: 成功,-1:失败]
     *
     * @return 处理状态code
     */
    default int getStatus() {
        return RequestStatus.SUCCESS;
    }

    /**
     * 处理状态code: [0: 成功,<0:失败]
     */
    default void getStatus(int status) {

    }
}