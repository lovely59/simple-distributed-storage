package com.lyon.demo.protocol.api.transport;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.protocol.api.core.Command;
import com.lyon.demo.protocol.api.transport.config.RemoteClientConfig;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;

import java.net.SocketAddress;
import java.time.Duration;

/**
 * 远程通信客户端
 *
 * @author LeeYan9
 * @since 2022-05-10
 */
@SuppressWarnings("all")
@SpiActivate(Protocols.NETTY)
public interface TransportClient<C, T extends Command, R extends Command, Config extends RemoteClientConfig> extends TransportService<C, T, Config> {

    /**
     * 创建远程通信通道
     *
     * @param socketAddress  远端地址
     * @param connectTimeout 连接超时
     * @return 通信通道
     */
    Transport<T, R> createIfAbsent(SocketAddress socketAddress, Duration connectTimeout);
}
