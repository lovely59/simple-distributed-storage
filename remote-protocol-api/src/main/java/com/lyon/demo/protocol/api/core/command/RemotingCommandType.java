package com.lyon.demo.protocol.api.core.command;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
public class RemotingCommandType {

    /**
     * 调用类型
     */
    public static final byte REQUEST = 0;
    public static final byte RESPONSE = 1;
}
