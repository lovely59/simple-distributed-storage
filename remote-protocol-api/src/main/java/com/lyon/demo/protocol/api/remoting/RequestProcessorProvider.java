package com.lyon.demo.protocol.api.remoting;

import com.lyon.demo.protocol.api.Protocols;
import com.lyon.demo.storage.common.spi.annotation.SpiActivate;

/**
 * @author LeeYan9
 * @since 2022-05-31
 */
@SuppressWarnings("rawtypes")
@SpiActivate(Protocols.NETTY)
public interface RequestProcessorProvider {

    /**
     * 通过 协议类型 确定处理器Class
//     * @param protocol 协议
     * @return
     */
    Class<? extends RequestProcessor> decisionProcessor();

}
