package com.lyon.demo.protocol.api.transport.config;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

/**
 * @author LeeYan9
 * @since 2022-05-31
 */
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PROTECTED)
@Accessors(chain = true)
public abstract class RemoteClientConfig {

    int listenPort = -1;

}
