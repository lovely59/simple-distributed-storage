package com.lyon.demo.protocol.api.remoting;

/**
 * @author LeeYan9
 * @since 2022-05-23
 */
public interface RpcHookProcessor<T> {

    /**
     * 前置处理请求
     *
     * @param command 请求参数
     */
    void preProcess(T command);

    /**
     * 后置处理请求
     *
     * @param command     请求参数
     * @param respCommand 请求结果
     */
    void postProcess(T command, T respCommand);

    /**
     * 获取rpc-Hook是否支持调用的谓语
     *
     * @return 谓语
     */
    RpcPredicate<T> predicate();
}