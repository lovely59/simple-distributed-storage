package com.lyon.demo.protocol.api.remoting;

/**
 * @author LeeYan9
 * @since 2022-05-31
 */
public interface Delegation<D> {

    /**
     * 设置委托处理器
     * @param delegate 处理器
     */
    void setDelegate(D delegate);

    /**
     * 设置委托处理器
     * @return  delegate 处理器
     */
    D getDelegate();

}
