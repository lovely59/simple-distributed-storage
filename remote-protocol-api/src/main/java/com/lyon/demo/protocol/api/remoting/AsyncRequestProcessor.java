package com.lyon.demo.protocol.api.remoting;

import com.lyon.demo.protocol.api.core.RemotingCallback;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
public interface AsyncRequestProcessor<C, T> extends RequestProcessor<C, T> {


    /**
     * 异步处理
     *
     * @param ctx      通道上下文上下文
     * @param command  请求参数
     * @param callback 执行回调
     * @return 结果
     */
    void asyncProcessRequest(C ctx, T command, RemotingCallback<T> callback);
}
