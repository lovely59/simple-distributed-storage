package com.lyon.demo.protocol.api.remoting;

@FunctionalInterface
public interface RpcPredicate<T> {
    boolean test(T command);
}