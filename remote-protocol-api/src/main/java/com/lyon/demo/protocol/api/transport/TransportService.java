package com.lyon.demo.protocol.api.transport;

import com.lyon.demo.protocol.api.remoting.RemotingService;
import com.lyon.demo.storage.common.thread.ShutdownAble;

import java.util.Properties;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
@SuppressWarnings("all")
public interface TransportService<C, T, Config> extends ShutdownAble, RemotingService<C, T> {

    /**
     * 准备请求接收服务端
     * 在 {@link TransportService#start()}执行前调用
     *
     * @param properties ext
     */
    void prepare(Properties properties);

    /**
     * 启动请求接收服务端
     */
    void start();

    void setConfig(Config config);
}