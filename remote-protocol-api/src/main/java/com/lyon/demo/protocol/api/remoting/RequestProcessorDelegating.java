package com.lyon.demo.protocol.api.remoting;

import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-05-31
 */
public interface RequestProcessorDelegating<T> {
    /**
     * 处理请求
     *
     * @param command 处理请求
     * @return 请求结果
     */
    T processRequest(T command);

    /**
     * 能够处理的一些请求
     *
     * @return RequestHandlerCode 列表
     */
    List<Integer> handlerCodes();
}
