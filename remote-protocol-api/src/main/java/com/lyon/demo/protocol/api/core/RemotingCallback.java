package com.lyon.demo.protocol.api.core;

/**
 * @author LeeYan9
 * @since 2022-05-24
 */
@FunctionalInterface
public interface RemotingCallback<R> {

    void callback(R command);

}
