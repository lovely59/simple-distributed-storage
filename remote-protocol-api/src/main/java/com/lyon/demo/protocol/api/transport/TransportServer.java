package com.lyon.demo.protocol.api.transport;

import com.lyon.demo.protocol.api.transport.config.RemoteClientConfig;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
@SuppressWarnings("all")
public interface TransportServer<CTX, T, Config extends RemoteClientConfig> extends TransportService<CTX, T, Config> {

}
