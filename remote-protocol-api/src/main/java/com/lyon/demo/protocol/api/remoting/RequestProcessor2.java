package com.lyon.demo.protocol.api.remoting;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface RequestProcessor2<C, T> extends RequestProcessor<C, T>{


    /**
     * 处理请求
     *
     * @param command 处理请求
     * @return 请求结果
     */
    T processRequest(T command);

}
