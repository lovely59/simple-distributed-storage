package com.lyon.demo.protocol.api.core;

/**
 * @author LeeYan9
 * @since 2022-05-25
 */
public interface RequestStatus {

    int SUCCESS = 0;
    int FAILURE = -1;

    int UNKNOWN_HANDLER_CODE = -100;

}
