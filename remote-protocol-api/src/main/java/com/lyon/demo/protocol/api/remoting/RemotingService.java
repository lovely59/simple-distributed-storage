package com.lyon.demo.protocol.api.remoting;

import java.util.concurrent.ExecutorService;

/**
 * @author LeeYan9
 * @since 2022-05-23
 */
public interface RemotingService<C, T> {

    /**
     * 添加处理器
     *
     * @param requestProcessor 协议处理器
     * @param executorService  线程池
     */
    void addRequestProcessor(RequestProcessor<C, T> requestProcessor, ExecutorService executorService);

    /**
     * 添加Rpc调用前置执行器
     *
     * @param rpcHookProcessor Rpc调用执行器
     */
    void addRpcHook(RpcHookProcessor<T> rpcHookProcessor);

    /**
     * 获取回调线程池
     * @param executorService 回调线程池
     */
    void setCallbackExecutor(ExecutorService executorService);

}
