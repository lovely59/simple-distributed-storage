package com.lyon.demo.protocol.api.remoting;


import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
public interface RequestProcessor<C, T> {

    /**
     * 处理请求
     *
     * @param ctx     通道上下文
     * @param command 命令
     * @return 结果
     */
    T processRequest(C ctx, T command);

    /**
     * 能够处理的一些请求
     *
     * @return RequestHandlerCode 列表
     */
    List<Integer> handlerCodes();
}
