package com.lyon.demo.protocol.api.core;

/**
 * @author LeeYan9
 * @since 2022-05-23
 */
public interface RemoteHandlerCodes {

    /**
     * 无效code
     */
    int ERROR_HANDLER_CODE = -1;

}
