package com.lyon.demo.storage.common.util;


import cn.hutool.core.thread.ThreadUtil;
import com.lyon.demo.storage.common.SystemClock;

/**
 * @author LeeYan9
 * @since 2022-05-19
 */
@SuppressWarnings("AlibabaClassNamingShouldBeCamel")
public class TimeUtil {

    public static long elapsed(long time) {
        return SystemClock.now() - time;
    }

    public static void sleep(int timeoutMs) {
        ThreadUtil.sleep(timeoutMs);
    }
}
