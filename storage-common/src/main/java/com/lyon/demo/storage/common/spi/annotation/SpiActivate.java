package com.lyon.demo.storage.common.spi.annotation;

import java.lang.annotation.*;

/**
 * 在接口定义时使用，可以声明默认使用的策略；
 * 在厂商实现上使用，可以声明当前使用的策略；
 * @author LeeYan9
 * @since 2022-04-15
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface SpiActivate {

    /**
     * spi 默认加载的策略类型
     *
     * @return 策略类型
     */
    String value();

}
