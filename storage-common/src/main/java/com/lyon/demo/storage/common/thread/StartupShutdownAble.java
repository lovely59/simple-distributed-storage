package com.lyon.demo.storage.common.thread;

/**
 * @author LeeYan9
 * @since 2022-05-27
 */
public interface StartupShutdownAble extends ShutdownAble {

    /**
     * 启动
     */
    void startup();

}
