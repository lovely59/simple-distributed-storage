package com.lyon.demo.storage.common.spi;

import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-04-15
 */
public interface SpiLoader {


    /**
     * 加载spi
     *
     * @param clazz
     * @return clazzList
     */
    <S> List<S> loader(Class<S> clazz);

    /**
     * 加载spi
     *
     * @param clazz
     * @return clazzList
     */
    <S> S loaderSingle(Class<S> clazz);

    /**
     * 加载spi
     *
     * @param clazz    clazz
     * @param strategy 策略
     * @return clazz
     */
    <S> S loader(Class<S> clazz, String strategy);

}
