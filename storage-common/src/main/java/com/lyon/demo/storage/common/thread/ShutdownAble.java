package com.lyon.demo.storage.common.thread;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public interface ShutdownAble {

    /**
     * 关闭
     */
    void shutdown();

}
