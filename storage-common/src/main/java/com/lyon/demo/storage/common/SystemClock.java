package com.lyon.demo.storage.common;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public class SystemClock {

    public static long now(){
        return System.currentTimeMillis();
    }

}
