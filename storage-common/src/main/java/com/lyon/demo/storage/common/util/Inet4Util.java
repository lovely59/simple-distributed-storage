package com.lyon.demo.storage.common.util;

import cn.hutool.core.text.CharSequenceUtil;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.List;

/**
 * @author LeeYan9
 * @since 2022-05-10
 */
public class Inet4Util {

    public static SocketAddress parseAddress(String address) {
        if (CharSequenceUtil.isBlank(address)) {
            return null;
        }
        List<String> split = CharSequenceUtil.split(address, ':');
        return new InetSocketAddress(split.get(0), Integer.parseInt(split.get(1)));
    }

}
