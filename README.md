# simple-distributed-storage


#### 介绍
 **基于Raft协议-DLedger的分布式K/V存储服务** 
-    基于最终一致性写入日志
-    支持选举、分区后重新选举、日志比较、截断
-    支持data、<K,V>方式写入日志


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
